package Interview;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
public class ALSorting1 {
	public static void main(String[] args) {
		ArrayList<Integer> alist=new ArrayList<Integer>(Arrays.asList(76,81,75,78,80,62,45,48,57,68,61,42,58,7861,66,70));
		Collections.sort(alist);
		System.out.println("After Ascending order sorting : ");
		for(int i:alist)
			System.out.print(i+" ");
		//Collections.sort(alist,Collections.reverseOrder());
		Collections.reverse(alist);
		System.out.println("\n After Descending order sorting : ");
		for(int i:alist)
			System.out.print(i+" ");
		
	}

}
