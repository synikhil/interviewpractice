package Interview;

import java.util.Scanner;

public class DecimalToOctal {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a decimal number : ");
		int decimal=sc.nextInt();
		int i=1,octal=0;
		while(decimal>0)
		{
			int r=decimal%8;
			octal=octal+r*i;
			i=i*10;
			decimal=decimal/8;
		}
		System.out.println("Octal = "+octal);	
	}
}
