package Interview;

public class Sorting2DArray {
	public static void main(String[] args) {
		int[][] arr={{16,15,14,13},{12,11,10,9},{8,7,6,5},{4,3,2,1}};
		int row=arr[0].length,column=4;
		int[] oneDArray=new int[row*column];
		for(int i=0,k=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				oneDArray[k]=arr[i][j];
				k++;
			}
		}
		for(int i=0,temp=0;i<oneDArray.length;i++)
		{
			for(int j=0;j<oneDArray.length-1-i;j++)
			{
				if(oneDArray[j]>oneDArray[j+1])
				{
					temp=oneDArray[j];
					oneDArray[j]=oneDArray[j+1];
					oneDArray[j+1]=temp;
				}
			}
		}
		for(int i=0,k=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				arr[i][j]=oneDArray[k];
				k++;
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
