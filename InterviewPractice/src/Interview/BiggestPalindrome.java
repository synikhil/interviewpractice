package Interview;

import java.util.Scanner;

public class BiggestPalindrome {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		String sentence="";
		String biggestPalin="";
		
		System.out.println("Enter a Sentence :");
		sentence=s.nextLine();
		String[] str=sentence.split(" ");
		System.out.println("Words : "+str.length);
		for(int i=0;i<str.length;i++)
		{
			String rev="";
			for(int j=str[i].length()-1;j>=0;j--)
			{
				rev=rev+str[i].charAt(j);
				
			}
			if(str[i].equals(rev)){
				if(str[i].length()>biggestPalin.length()){
					biggestPalin=str[i];
				}
			}
				
		}
		System.out.println("Biggest Palindrome in a given sentence : "+biggestPalin);
	}
}