package Interview;
public class MatrixDiagonal1 {
		public static void main(String[] args) {
			int a[][]={{1,2,3,4},
					{5,6,7,8},
					{9,10,11,12},
					{13,14,15,16}};
			int diagonal1[]=new int[a.length];
			int diagonal2[]=new int[a.length];
			for(int i=0;i<a.length;i++){
				for(int j=0;j<a.length;j++){
					if(i==j)
						diagonal1[i]=a[i][j];
					if(i+j==a.length-1)
						diagonal2[i]=a[i][j];
				}
			}
			System.out.println("One side");
			for(int irr:diagonal1)
				System.out.print(irr+" ");
			System.out.println("\nOther side");
			for(int irr:diagonal2)
				System.out.print(irr+" ");
	}
}
