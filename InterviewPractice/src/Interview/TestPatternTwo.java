package Interview;

import java.util.Scanner;

public class TestPatternTwo {
	public static void main(String[]args)
	{
		int row,num=1;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of row : ");
		row=sc.nextInt();
		if(row%2==0)
		{ 
			int i;
			for(i=1;i<=row/2;i++)
			{
				for(int j=1;j<=(i*2)-1;j++)
				{
					if(j%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(num);
						num++;
					}
				}
				System.out.println();
			}
			num=num-(i-1);
			int temp=i-2;
			i=i-1;
			for(int j=row/2;j>=1;j--)
			{
				for(int k=(j*2)-1;k>=1;k--)
				{
					if(k%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(num);
						num++;
					}
				}
				System.out.println();
				num=num-(i+temp);
				temp--;
				i--;
			}
		}
		else
		{
			int i;
			for(i=1;i<=(row/2)+1;i++)
			{
				for(int j=1;j<=(i*2)-1;j++)
				{
					if(j%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(num);
						num++;
					}
				}
				System.out.println();
			}
			int t=i-3;
			num=num-(i+t);
			i=i-2;
			for(int x=row/2;x>=1;x--)
			{
				for(int y=1;y<=(2*x)-1;y++)
				{
					if(y%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(num);
						num++;
					}
				}
				System.out.println();
				num=num-(i+t);
				t--;
				i--;
			}	
		}
	}

}
