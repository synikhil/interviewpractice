package Interview;

public class TwoDArrayTo1D {
	public static void main(String[] args) {
		int[][] arr={{3,4,1,6},{9,4,7,5},{0,9,2,8}};
		int row=3,column=4;
		int[] arr1=new int[row*column];
		for(int i=0,k=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				arr1[k]=arr[i][j];
				k++;
			}
		}
		System.out.println("After converting 2D into 1D :)  ");
		for(int irr:arr1)
			System.out.print(irr+" ");
		
		int temp=0;
		for(int i=0;i<row*column;i++)
		{
			for(int j=0;j<row*column-1-i;j++)
			{
				if(arr1[j]>arr1[j+1])
				{
					temp=arr1[j];
					arr1[j]=arr1[j+1];
					arr1[j+1]=temp;
				}
			}
		}
		System.out.println("\nAfter Sorting 1D Array :) ");
		for(int irr:arr1)
			System.out.print(irr+" ");
	}
}
