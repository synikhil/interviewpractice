package Interview;

public class CountObjects {
    static int count=0;
    
  //instance block
    
	{        
		count++;
	}
	
	
	CountObjects()
	{
	}
	CountObjects(int i)
	{
	}
	CountObjects(double d)
	{
	}
	public static void main(String[] args) {
		CountObjects obj1=new CountObjects();
		CountObjects obj2=new CountObjects(10);
		CountObjects obj3=new CountObjects(10.5);
		System.out.println("The number of objects created : "+count);		
	}
}
