package Interview;

public class GreatestIn2DArray {
	public static void main(String[] args) {
		int[][] arr={{70,42,29,37},{61,66,81,76},{54,60,68,94},{78,80,107,181}};
		int grtst=arr[0][0];
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				if(grtst<arr[i][j])
				{
					grtst=arr[i][j];
				}
			}
		}
		System.out.println("Greatest among all elements is "+grtst);
	}

}
