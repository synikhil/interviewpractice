package Interview;

public class BubbleSort {
	public static void main(String[] args) {
		int arr[]={45,42,37,29,70,76,75,81,26,23,47,66,61,62};
		int temp=0;
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length-1-i;j++)
			{
				if(arr[j]<arr[j+1])
				{
					temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		for(int irr:arr)
			System.out.print(irr+" ");
	}

}
