package Interview;

public class LargestElementColumn {
	public static void main(String[] args) {
		int largest=0,smallest=0;
		int[][] arr={{70,42,12,37},{61,66,81,76},{54,60,68,94},{12,28,99,42}};
		//Code for Displaying existing matrix
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				System.out.print(arr[i][j]+" ");
			}
			
			System.out.println();
		}
		System.out.println("\n\n");
		//Coding for requirement
		for(int i=0;i<arr.length;i++)
		{
			largest=arr[0][i];smallest=arr[0][i];
			for(int j=0;j<arr.length;j++)
			{
				if(largest<arr[j][i])
				{	
					largest=arr[j][i]; 
					
				}
				if(arr[j][i]<smallest)
				{
					smallest=arr[j][i];
					
				}
			}
			System.out.println("Column : "+( i+1)+"   Largest : "+largest+"    Smallest : "+smallest);
		}
	}
}
