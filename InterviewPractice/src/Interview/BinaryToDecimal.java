package Interview;

import java.util.Scanner;

public class BinaryToDecimal {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Enter a Binary number : ");
		int binary=in.nextInt();
		int r,i=1,decimal=0;
		while(binary>0)
		{
			r=binary%10;
			decimal=decimal+(r*i);
			i=i*2;
			binary=binary/10;
		}
		System.out.println("Decimal = "+decimal);
	}
}
