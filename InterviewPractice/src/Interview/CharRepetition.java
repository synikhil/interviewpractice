package Interview;

import java.util.Scanner;

public class CharRepetition {
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a String : ");
		String str=sc.nextLine();
		int count=0;
		for(int i=0;i<str.length();i++)
		{
			count=0;
			for(int j=0;j<str.length();j++)
			{
				if(str.charAt(i)==str.charAt(j))
				{
					if(j<i)
						break;
					else
						count++;
				}
			}
			if(count>1)
				 System.out.println("Duplicate character : "+str.charAt(i)+"  "+"Repetition : "+count);
		}
		
			 
	}
}
