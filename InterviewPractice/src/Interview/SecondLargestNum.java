package Interview;


public class SecondLargestNum {
	public static void main(String[]args)
	{
		int a[]={10,2,8,3,5,11,4,15};
		int Largest=a[0];
		int secondLargest=0;
		int smallest=a[0];
		int secondSmallest=a[0];
		for(int i=1;i<a.length;i++)
		{
			if(a[i]>Largest)
			{
				secondLargest=Largest;
				Largest=a[i];
			}
			if(a[i]<smallest)
				smallest=a[i];
		}
		for(int i=1;i<a.length;i++)
		{
			if(a[i]<secondSmallest&& smallest!=a[i])
			{
				secondSmallest=a[i];
			}
		}
		
		System.out.println("Second largest : "+secondLargest+"      "+"Second smallest : "+secondSmallest);
		
	}

}
