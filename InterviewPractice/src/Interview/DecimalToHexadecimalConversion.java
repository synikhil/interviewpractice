package Interview;

import java.util.Scanner;

public class DecimalToHexadecimalConversion {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a Decimal number : ");
		int dec=sc.nextInt();
		String str="";
		char[] c={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
		while(dec>0){
			int r=dec%16;
			str=c[r]+str;
			dec=dec/16;
		}
		System.out.println("Hexadecimal = "+str);
	}
}
