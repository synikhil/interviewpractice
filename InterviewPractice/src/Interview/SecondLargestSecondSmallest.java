package Interview;

public class SecondLargestSecondSmallest {
	public static void main(String[] args) {
		int a[]={10,2,8,3,5,11,4,15};
		int temp=0;
		for(int i=0;i<a.length;i++)	
		{
			for(int j=i+1;j<a.length;j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		System.out.println("Second Largest = "+a[a.length-2]+"\n"+"Second Smallest = "+a[1]);	
	}

}
