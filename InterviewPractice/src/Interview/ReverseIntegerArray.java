package Interview;

public class ReverseIntegerArray {
	public static void main(String[] args) {
		int a[]={15,13,37,25,10,45  };
		int temp=0;
		for(int i=0;i<a.length/2;i++)
		{
			temp=a[i];
			a[i]=a[a.length-1-i];
			a[a.length-1-i]=temp;
		}
		for(int i:a)
			System.out.print(i+" ");
	}

}
