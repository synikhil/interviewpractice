package Interview;

import java.util.Scanner;

public class OctalToDecimal {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a Octal number : ");
		int octal=sc.nextInt();
		int decimal=0,i=1;
		while(octal>0)
		{
			int r=octal%10;
			decimal=decimal+r*i;
			i=i*8;
			octal=octal/10;
		}
		System.out.println("Decimal = "+decimal);
	}

}
