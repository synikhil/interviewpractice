package Interview;

import java.util.ArrayList;
import java.util.Collections;

public class ALSorting {
	public static void main(String[] args) {
		ArrayList<String> cousins=new ArrayList<String>();
		cousins.add("Nikhil");
		cousins.add("Ajay");
		cousins.add("Arunjay");
		cousins.add("Kaushal");
		cousins.add("Cintu");
		cousins.add("Bablu");
		cousins.add("Mantu");
		cousins.add("Antu");
		System.out.println("Before sorting : ");
		for(String str:cousins)
			System.out.print(str+"  ");
		Collections.sort(cousins);
		System.out.println("\nAfter sorting :");
		for(String str:cousins)
			System.out.print(str+"  ");
		
		
	}

}
