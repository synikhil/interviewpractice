package Interview;

public class MatrixDiagonal {
	public static void main(String[] args) {
		int[][]arr={{10,11,12,13},{14,15,16,17},{18,19,20,21},{22,23,24,25}};
		System.out.println("Diagonal Elements : ");
		for(int i=0;i<arr.length;i++)
		{
				System.out.print(arr[i][i]+" ");
		}
		System.out.println("\nAnti Diagonal Elements : ");
		for(int i=0,j=arr.length-1;i<arr.length;i++,j--)
		{
				System.out.print(arr[i][j]+" ");	
		}
	}

}
