package Interview;

import java.util.ArrayList;

public class ALSize {
	public static void main(String[] args) {
		char c='A';
		System.out.println(c=='A');
		ArrayList<Integer> al=new ArrayList<Integer>();
		System.out.println("Initial size of ArrayList:"+al.size());
		al.add(4);
    	al.add(6);
    	al.add(33);
    	al.add(19);
    	al.add(83);
    	al.add(68);
    	al.add(76);
    	al.add(62);
    	System.out.println("Initial  ArrayList  "+al);
    	System.out.println(" Size of ArrayList after few additions:"+al.size());
    	al.remove(1);
    	al.remove(4);
    	System.out.println("ArrayList size after remove operation:"+al.size());
    	System.out.println("Final ArrayList :  "+al);
    	for(int i:al)
    		System.out.println("Final AL : "+i);
    	al.clear();
    	System.out.println("Size of ArrayList after clear operation:"+al.size());  	
  	}
}

