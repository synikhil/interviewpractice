package Interview;
import java.util.Scanner;
public class ReverseString {
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		String str,rev="";
		
		System.out.println("Enter a string: ");
		str=sc.nextLine();
		for(int i=str.length()-1;i>=0;i--){
			rev=rev+str.charAt(i);
		}
		System.out.println("String : "+str);
		System.out.println("It's Reverse : "+rev);
		
	}

}
