package Interview;

public class EqualMatrix {
	public static void main(String[] args) {
		int[][] matrix1={{1,2,3},{4,5,6},{7,8,9}};
		int[][] matrix2={{1,2,3},{4,5,6}};
		if(matrix1.length==matrix2.length&&matrix1[0].length==matrix2[0].length)
		{
			int count=0;
			for(int i=0;i<matrix1.length;i++)
			{
				for(int j=0;j<matrix1[0].length;j++)
				{
					if(matrix1[i][j]==matrix2[i][j])
					{
						count++;
					}
					
				}
				
			}
			if(count==matrix1.length*matrix2[0].length)
			{
				System.out.println("Given matrices are equal.");
			}
			else
			{
				System.out.println("Given matrices are not equal.");
			}
		}
		else
		{
			System.out.println("Uncomparable matrices are not valid for equality.");
		}
	}
}
