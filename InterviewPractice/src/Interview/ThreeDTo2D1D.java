package Interview;

public class ThreeDTo2D1D {
	public static void main(String[] args) {
		int arr[][][]={
				{{1,2,3},{4,5,6}},
				{{5,6},{6,8},{7,9}}
				};
		int twoDArray=2;
		int[][] arr1=arr[0];
		int row1=2,column1=3;
		int[][] arr2=arr[1];
		int row2=arr[1].length,column2=2;
		int[] oneDArray=new int[(row1*column1)+(row2*column2)];
		for(int n=0,k=0;n<twoDArray;n++)
		{
			if(n==0)
			{
				System.out.println("\nFirst 2D Array : ");
				for(int i=0;i<row1;i++)
				{
					for(int j=0;j<column1;j++)
					{
//						arr1[i][j]=arr[n][i][j];
						oneDArray[k]=arr1[i][j];
						k++;
						System.out.print(arr1[i][j]+" ");
					}
					System.out.println();
				}
			}
			else
			{
				System.out.println("\nSecond 2D Array : ");
				for(int i=0;i<row2;i++)
				{
					for(int j=0;j<column2;j++)
					{
//						arr2[i][j]=arr[n][i][j];
						oneDArray[k]=arr2[i][j];
						k++;
						System.out.print(arr2[i][j]+" ");
					}
					System.out.println();
				}
			}	
		}
		System.out.println("\nOne Dimension Array : ");
		for(int irr:oneDArray)
			System.out.print(irr+" ");
   }

}
