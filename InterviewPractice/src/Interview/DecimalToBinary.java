package Interview;

import java.util.Scanner;

public class DecimalToBinary {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a decimal number : ");
		int decimal=sc.nextInt();
		
		int i=1,binary=0;
		while(decimal>0)
		{
			int r=decimal%2;
			binary=binary+r*i;
			i=i*10;
			decimal=decimal/2;		
			
		}
		System.out.println("Binary = "+binary);
	}
}
