
package Interview;

import java.util.Scanner;

public class PatternCheckOne {
	public static void main(String[]args){
		int num=1,temp,row;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		row=sc.nextInt();
		for(int i=1;i<=row;i++)
		{
			if(i%2==0)
			{
				temp=num;
				int k=i+(temp-1);
				for(int j=1;j<=(i*2)-1;j++)
				{
					if(j%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(k);
						k--;
						num=num+1;
					}
				}			
			}
			else
			{
				for(int j=1;j<=(i*2)-1;j++)
				{
					if(j%2==0)
						System.out.print(" * ");
					else
					{
						System.out.print(num);
						num=num+1;
					}
				}	
			}
			System.out.println();
		}
		
		
	}

}
