package Interview;
import java.util.*;

public class ALCreationAndAddition
{
	public static void main(String[]args)
	{    
		      ArrayList<String> alist=new ArrayList<String>();  
		      alist.add("Steve");
		      alist.add("Tim");
		      alist.add("Lucy");
		      alist.add("Pat");
		      alist.add("Angela");
		      alist.add("Tom");
		  
		      //displaying elements
		      System.out.println(alist);
		  
		      //Adding "Steve" at the fourth position
		      alist.add(3, "Nikhil");
		  
		      //displaying elements
		      System.out.println(alist);
		}  
}


