package Interview;

public class LargestIn2D {
	public static void main(String[] args) {
		int[][] arr={{70,42,29,37},{61,66,81,76},{54,60,68,94},{78,80,107,181}};
		int largest=0,smallest=0;
		//Code for displaying hard coded matrix.
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				System.out.print(arr[i][j]+" ");
			}
			
			System.out.println();
		}
		System.out.println("\n\n");
		//Code for requirement.
		for(int i=0;i<arr.length;i++)
		{
			largest=arr[i][0]; smallest=arr[i][0];
			for(int j=0;j<arr.length;j++)
			{
				if(largest<arr[i][j])
				{
					largest=arr[i][j];
				}
				if(smallest>arr[i][j])
					smallest=arr[i][j];
			}
			System.out.println("Row : "+( i+1)+"   Largest : "+largest+"   Smallest : "+smallest);
		}
	}
}
